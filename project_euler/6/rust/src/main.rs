fn difference_between_sum_of_squares_and_sum_of_first_n_natural_numbers(n: u64) -> u64 {
    /*
    Difference between A and B^2, given that:

    Sum of all natural numbers up to n:
    B = n*(n + 1)/2

    Sum of the squares of all natural numbers up to n:
    A = n(n + 1)(2n + 1)/6

    # A = n * (n + 1) * ((2 * n) + 1) / 6
    # B = n * (n + 1) / 2
    # B_squared = B ** 2
    */

    //Algebraically simplified:
    let result: u64 = 3 * (n.pow(4) - n.pow(2)) + 2 * (n.pow(3) - n);
    return result / 12;
}

fn main() {
    println!(
        "{:?}",
        difference_between_sum_of_squares_and_sum_of_first_n_natural_numbers(100)
    );
}
