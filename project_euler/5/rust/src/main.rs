fn is_evenly_divisible_up_to(input: u64, n: u32) -> bool {
    for x in 2..n + 1 {
        if input % x as u64 != 0 {
            return false;
        }
    }
    true
}

fn main() {
    let mut n: u64 = 4_849_846;
    while !is_evenly_divisible_up_to(n, 20) {
        n += 2
    }
    println!("{:?}", n);
}
