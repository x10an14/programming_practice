use std::env;
use std::error::Error;
use std::process::exit;

extern crate slice_deque;

const INPUT_NUMS: &str = "\
73167176531330624919225119674426574742355349194934\
96983520312774506326239578318016984801869478851843\
85861560789112949495459501737958331952853208805511\
12540698747158523863050715693290963295227443043557\
66896648950445244523161731856403098711121722383113\
62229893423380308135336276614282806444486645238749\
30358907296290491560440772390713810515859307960866\
70172427121883998797908792274921901699720888093776\
65727333001053367881220235421809751254540594752243\
52584907711670556013604839586446706324415722155397\
53697817977846174064955149290862569321978468622482\
83972241375657056057490261407972968652414535100474\
82166370484403199890008895243450658541227588666881\
16427171479924442928230863465674813919123162824586\
17866458359124566529476545682848912883142607690042\
24219022671055626321111109370544217506941658960408\
07198403850962455444362981230987879927244284909188\
84580156166097919133875499200524063689912560717606\
05886116467109405077541002256983155200055935729725\
71636269561882670428252483600823257530420752963450\
";

fn to_digits(text: &str) -> Option<Vec<u32>> {
    text.chars()
        .map(|character| character.to_digit(10))
        .collect()
}

fn main() -> Result<(), Box<dyn Error>> {
    // println!("Hello, world!");
    // println!("{}", INPUT_NUMS);
    //let input_nums: Vec<u32> = vec![1, 2, 3, 4, 5, 6, 7, 8];
    let input_nums: Vec<u32> = to_digits(INPUT_NUMS).unwrap_or_else(|| {
        eprintln!("The input string of 1000 numbers cannot be made into a Vec<u32>!");
        exit(1);
    });
    //println!("{}", input_nums.len());

    let args: Vec<String> = env::args().collect();
    let number_of_sequential_factors: usize = args
        .get(1)
        .unwrap_or_else(|| {
            eprintln!("You must specify the amount of adjacent numbers to multiply!");
            exit(1);
        })
        .parse::<usize>()
        .unwrap_or_else(|_| {
            eprintln!("The amount adjacent numbers to multiply must be a positive integer...");
            exit(1);
        });
    // println!("{}", number_of_sequential_factors);

    let mut deq = slice_deque::SliceDeque::<&u32>::with_capacity(number_of_sequential_factors);
    // println!("{:?}", deq);

    let mut max_product: u128 = 0;
    let mut max_product_factors: Vec<&u32> = Vec::with_capacity(number_of_sequential_factors);
    for number in &input_nums {
        if deq.len() < number_of_sequential_factors {
            deq.push_front(&number);
            continue;
        } else if deq.len() == number_of_sequential_factors {
            let _ = deq.pop_back();
        }
        deq.push_front(&number);
        let new_product: u128 = deq.iter().fold(1u128, |acc, &num| acc * (*num as u128));
        if new_product > max_product {
            max_product = new_product;
            let slice = &deq[..number_of_sequential_factors];
            if max_product_factors.len() == 0 {
                max_product_factors.extend_from_slice(slice);
            } else {
                max_product_factors.copy_from_slice(slice);
            }
        }
    }
    eprintln!(
        "The max product of the following {:?} adjacent factors becomes;",
        &max_product_factors
    );
    println!("{}", max_product);
    Ok(())
}
