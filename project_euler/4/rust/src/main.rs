fn is_string_palindrome(input_string: String) -> bool {
    // https://stackoverflow.com/a/27996791
    let reverse_string: String = input_string
        .chars()
        .rev()
        .collect();
    input_string == reverse_string
}

fn is_number_palindrome(number: u64) -> bool {
    let num_string = number.to_string();
    is_string_palindrome(num_string)
}

fn largest_palindromic_product_of_two_n_digit_multiplicands(n: u32) -> u64 {
    let max_multiplicand = 10_u64.pow(n) - 1;
    let mut largest_palindrome: u64 = 9;
    let mut x: u64 = max_multiplicand;

    println!("{:?} {:?} {:?}", max_multiplicand, x, largest_palindrome);
    while x.to_string().len() == n as usize {
        let mut y = max_multiplicand;
        while y.to_string().len() == n as usize {
            let product: u64 = x * y;
            if product <= largest_palindrome {
                break;
            }
            if is_number_palindrome(product) && product > largest_palindrome {
                println!("{:?} {:?} {:?}", x, y, product);
                largest_palindrome = product;
            }
            y -= 1;
        }
        x -= 1;
    }
    largest_palindrome
}

fn main() {
    println!("{}", largest_palindromic_product_of_two_n_digit_multiplicands(3));
    // println!("{}", is_number_palindrome(20102));
}
