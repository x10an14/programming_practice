extern crate primes;
use primes::PrimeSet;

fn main() {
    let mut prime_set = PrimeSet::new();
    let nth: usize = 10_001;
    let nth_prime: u64 = prime_set.get(nth - 1); // 0-indexed
    println!("{:#?}", nth_prime);
}
